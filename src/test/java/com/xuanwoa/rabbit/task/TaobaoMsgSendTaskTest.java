package com.xuanwoa.rabbit.task;

import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Description
 *
 * @author caomin
 * @since 2020/12/27
 */
@Slf4j
@SpringBootTest
class TaobaoMsgSendTaskTest {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    void sendDelayQueue() {
        for (int i = 0; i < 10; i++) {
            rabbitTemplate.convertAndSend("demo_delay_exchange", "", DateUtil.now(), message -> {
                message.getMessageProperties().setHeader("x-delay", 1000);
                return message;
            });
            log.info("发送延迟消息=====");
        }
    }
}