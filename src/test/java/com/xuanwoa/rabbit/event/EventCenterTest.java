package com.xuanwoa.rabbit.event;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Description
 *
 * @author: caomin
 * @since: 2020/11/30
 */
class EventCenterTest {
    @Test
    void test() {
        EventCenter eventCenter = new EventCenter();
        eventCenter.register(new ObserverA());
        eventCenter.register(new ObserverB());
        eventCenter.post(1);
        eventCenter.post("hello");
    }
}