package com.xuanwoa.rabbit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
public class RabbitDemoApplication {


    //v2
    public static void main(String[] args) {
        SpringApplication.run(RabbitDemoApplication.class, args);
    }

}
