package com.xuanwoa.rabbit.hutool.clone;

import cn.hutool.core.clone.CloneSupport;
import cn.hutool.core.util.ObjectUtil;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/21
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User extends CloneSupport<User> implements Serializable {
    Integer age;

    String name;

    //TODO 待深入
    public static void main(String[] args) {
        User user = new User();
        user.setAge(23);
        user.setName("xuanwoa");
        User cloneUser = user.clone();
        System.out.println(ObjectUtil.notEqual(user.hashCode(), cloneUser.hashCode()));

        User cloneByStream = ObjectUtil.cloneByStream(user);
        System.out.println(ObjectUtil.notEqual(user.hashCode(), cloneByStream.hashCode()));

    }
}
