package com.xuanwoa.rabbit.hutool.convert;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.Console;

import java.util.Date;
import java.util.List;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/21
 */
public class ConvertDemo {
    public static void main(String[] args) {
        Object[] a = {"a", "你好", "", 1};
//        List<?> list = Convert.convert(List.class, a);
        List<?> list = Convert.toList(a);
        System.out.println(list);
    }

    private static void toDate() {
        String a = "2021-01-21";
        Date date = Convert.toDate(a);
        System.out.println(date);
    }

    private static void toArray() {
        String[] b = {"1", "2", "3", "4"};
        Integer[] intArray = Convert.toIntArray(b);
        long[] c = {1, 2, 3, 4, 5};
        Integer[] intArray1 = Convert.toIntArray(c);
    }

    private static void toStr() {
        int a = 1;
        String aStr = Convert.toStr(a);
        Console.log(aStr);
        long[] b = {1, 2, 3, 4, 5, 6};
        String bStr = Convert.toStr(b);
        Console.log(bStr);
    }
}
