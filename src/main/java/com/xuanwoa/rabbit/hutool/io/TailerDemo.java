package com.xuanwoa.rabbit.hutool.io;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.Tailer;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/21
 */
public class TailerDemo {
    public static void main(String[] args) {
        Tailer tailer = new Tailer(FileUtil.file("/Users/xuanwoa/未命名.txt"), Tailer.CONSOLE_HANDLER, 0);
        tailer.start();
    }
}
