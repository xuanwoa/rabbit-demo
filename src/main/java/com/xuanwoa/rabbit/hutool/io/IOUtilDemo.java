package com.xuanwoa.rabbit.hutool.io;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.lang.Console;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/21
 */
public class IOUtilDemo {
    public static void main(String[] args) {
        BufferedInputStream in = FileUtil.getInputStream("/Users/xuanwoa/Downloads/Magnet_2.5.0_(90)_(最低10.10)__macwk.com.dmg");
        BufferedOutputStream out = FileUtil.getOutputStream("/Users/xuanwoa/Downloads/Magnet_2.5.0_(90)_(最低10.10)__macwk.com_bak.dmg");
        long copySize = IoUtil.copy(in, out, IoUtil.DEFAULT_BUFFER_SIZE);
        Console.log("copySize[{}]", copySize);
        File[] ls = FileUtil.ls("/");
        for (File file : ls) {
            System.out.println(file.getName());
        }
    }
}
