package com.xuanwoa.rabbit.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * Description
 *
 * @author: caomin
 * @since: 2020/12/1
 */
@Component
@Slf4j
public class DemoRunnerA implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info(args.toString());
    }
}
