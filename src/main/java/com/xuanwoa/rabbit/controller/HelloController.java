package com.xuanwoa.rabbit.controller;

import com.xuanwoa.rabbit.MyException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Description
 *
 * @author: caomin
 * @since: 2020/12/1
 */
@RestController
@Slf4j
public class HelloController {

    @AllArgsConstructor
    @Data
    static class User {
        private Long id;
        private String username;
    }

    @GetMapping("user")
    public List<User> listUser() {
//        return List.of(new User(1L, "xuanwoa"));
        return null;
    }

    @GetMapping("query")
    public User queryUser(Long id, String name) {
        return new User(id, name);
    }

    @PostMapping("user")
    public User addUser(@RequestBody User user) {
        return user;
    }

    @GetMapping("exception")
    public String exception(Integer type) throws MyException {
        if (type.equals(1)) {
            throw new MyException();
        } else {
            throw new NullPointerException();
        }
    }
}
