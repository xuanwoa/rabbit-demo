package com.xuanwoa.rabbit.task;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.xuanwoa.rabbit.constant.RabbitConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Description
 *
 * @author caomin
 * @since 2020/12/26
 */
@Component
@Slf4j
public class TaobaoMsgSendTask {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Scheduled(cron = "0/2 * * * * ?")
    public void sendTrade() {
        String msgTemplate = "{\n" +
                "    \"topic\": \"taobao_trade_TradeBuyerPay\",\n" +
                "    \"content\": \"{\\\"tid\\\": 1469094337286414817,\\\"oid\\\": 1469094337286414817, \\\"seller_nick\\\": \\\"babycare旗舰店\\\",\\\"buyer_nick\\\": \\\"tb85406790\\\",\\\"type\\\": \\\"fixed\\\",\\\"status\\\": \\\"WAIT_SELLER_SEND_GOODS\\\",\\\"payment\\\": \\\"12.32\\\"}\"\n" +
                "}";
        rabbitTemplate.convertAndSend(RabbitConstant.TAOBAO_EXCHANGE, "", msgTemplate);
    }

    @Scheduled(cron = "0/2 * * * * ?")
    public void sendRefund() {
        String msgTemplate = "{\n" +
                "    \"topic\": \"taobao_refund_RefundSuccess\",\n" +
                "    \"content\": \"{\\\"tid\\\": 1469094337286414817,\\\"oid\\\": 1469094337286414817,\\\"refund_id\\\": 95122838612166888, \\\"seller_nick\\\": \\\"babycare旗舰店\\\", \\\"bill_type\\\": \\\"refund_bill\\\",\\\"buyer_nick\\\": \\\"赵杨753951\\\",\\\"refund_fee\\\": \\\"29.90\\\",\\\"modified\\\": \\\"2020-12-25 02:22:21\\\"}\"\n" +
                "}";
        rabbitTemplate.convertAndSend(RabbitConstant.TAOBAO_EXCHANGE, "", msgTemplate);
    }

//    @Scheduled(cron = "0 0/1 * * * ?")
//    @Scheduled(cron = "0/2 * * * * ?")
    public void sendDelayQueue() {
        rabbitTemplate.convertAndSend("demo_delay_exchange", "", DateUtil.now(), message -> {
            message.getMessageProperties().setHeader("x-delay", 1000 * 60);
            return message;
        });
        log.info("发送延迟消息=====");
    }

}
