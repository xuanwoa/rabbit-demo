package com.xuanwoa.rabbit.spi.impl;

import com.xuanwoa.rabbit.spi.SpiService;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/21
 */
public class ASpiServiceImpl implements SpiService {
    @Override
    public void printService() {
        System.out.println(this.getClass().getName());
    }
}
