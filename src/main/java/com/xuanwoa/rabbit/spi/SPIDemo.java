package com.xuanwoa.rabbit.spi;

import java.util.ServiceLoader;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/21
 */
public class SPIDemo {
    public static void main(String[] args) {
        ServiceLoader<SpiService> spiServices = ServiceLoader.load(SpiService.class);
        for (SpiService spiService : spiServices) {
            spiService.printService();
        }
    }
}
