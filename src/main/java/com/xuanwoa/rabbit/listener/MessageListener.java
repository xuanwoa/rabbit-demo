package com.xuanwoa.rabbit.listener;

import com.xuanwoa.rabbit.constant.RabbitConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Description
 *
 * @author: xuanwoa
 * @since: 2020/11/27
 */
@Component
@Slf4j
@Profile("receive")
public class MessageListener {
    private AtomicInteger atomicInteger = new AtomicInteger(0);

    @RabbitListener(queues = {RabbitConstant.DIRECT_QUEUE_A})
    public void receiveA(String msg) {
        atomicInteger.addAndGet(1);
        log.info("received queue[{}] msg[{}],index[{}]", RabbitConstant.DIRECT_QUEUE_A, msg, atomicInteger.get());
    }

    @RabbitListener(queues = {RabbitConstant.FANOUT_QUEUE_B})
    public void receiveB(String msg) {
        log.info("received queue[{}] msg[{}]]", RabbitConstant.FANOUT_QUEUE_B, msg);
    }

    @RabbitListener(queues = {RabbitConstant.FANOUT_QUEUE_C})
    public void receiveC(String msg) {
        log.info("received queue[{}] msg[{}]]", RabbitConstant.FANOUT_QUEUE_C, msg);
    }

    @RabbitListener(queues = {RabbitConstant.TOPIC_QUEUE_D})
    public void receiveD(String msg) {
        log.info("received queue[{}] msg[{}]]", RabbitConstant.TOPIC_QUEUE_D, msg);
    }

    @RabbitListener(queues = {RabbitConstant.TOPIC_QUEUE_E})
    public void receiveE(String msg) {
        log.info("received queue[{}] msg[{}]]", RabbitConstant.TOPIC_QUEUE_E, msg);
    }
}
