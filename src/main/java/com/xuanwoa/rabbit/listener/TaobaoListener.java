package com.xuanwoa.rabbit.listener;

import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Description
 *
 * @author caomin
 * @since 2020/12/27
 */
@Slf4j
@Component
public class TaobaoListener {
    @RabbitListener(queues = {"demo_delay_queue"}, concurrency = "1-4")
    public void handle(String msg) {
        log.info("收到延迟消息[{}]，当前时间[{}]", msg, DateUtil.now());
    }
}
