package com.xuanwoa.rabbit.aop;

import com.xuanwoa.rabbit.MyException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * Description
 *
 * @author: caomin
 * @since: 2020/12/1
 */
@Component
@Aspect
public class ExceptionHandler {
    @Pointcut("execution (public * com.xuanwoa.rabbit.controller.*.*(..))")
    public void pointcut() {

    }

    @AfterThrowing(pointcut = "pointcut()",throwing = "e")
    public void handlerException(JoinPoint joinPoint, MyException e) {
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        //开始打log
        System.out.println("异常:" + e.getMessage());
        System.out.println("异常所在类：" + className);
        System.out.println("异常所在方法：" + methodName);
        System.out.println("异常中的参数：");
        System.out.println(methodName);
        for (int i = 0; i < args.length; i++) {
            System.out.println(args[i].toString());
        }
    }
}
