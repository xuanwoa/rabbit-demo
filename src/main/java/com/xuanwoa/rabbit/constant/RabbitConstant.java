package com.xuanwoa.rabbit.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Description
 *
 * @author: xuanwoa
 * @since: 2020/11/27
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RabbitConstant {
    public static final String DIRECT_ROUTING_KEY = "direct.key";

    public static final String DIRECT_EXCHANGE = "direct_exchange";

    public static final String FANOUT_EXCHANGE = "fanout_exchange";

    public static final String TOPIC_EXCHANGE = "topic_exchange";

    public static final String TAOBAO_EXCHANGE = "taobao_exchange";

    public static final String DIRECT_QUEUE_A = "direct_queue_a";

    public static final String FANOUT_QUEUE_B = "direct_queue_b";

    public static final String FANOUT_QUEUE_C = "direct_queue_c";

    public static final String TOPIC_QUEUE_D = "topic_queue_d";

    public static final String TOPIC_QUEUE_E = "topic_queue_e";

    public static final String TAOBAO_QUEUE = "taobao_queue";
}
