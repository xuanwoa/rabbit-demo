package com.xuanwoa.rabbit.guava.util;

import com.google.common.collect.ForwardingList;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/20
 */
@Slf4j
public class LogInfoForwardingList<T> extends ForwardingList<T> {
    final List<T> delegate; // backing list

    public LogInfoForwardingList() {
        delegate = new ArrayList<>();
    }


    @Override
    protected List<T> delegate() {
        return delegate;
    }

    @Override
    public void add(int index, T element) {
        log.info("index[{}],element[{}]", index, element);
        super.add(index, element);
    }

    public static void main(String[] args) {
        LogInfoForwardingList<Integer> list = new LogInfoForwardingList();
        list.add(0,1);
        System.out.println(list);
    }
}
