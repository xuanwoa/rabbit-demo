package com.xuanwoa.rabbit.guava.util;

import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.PeekingIterator;

import java.util.Iterator;
import java.util.List;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/20
 */
public class PeekingIteratorDemo {
    public static void main(String[] args) {
        List list = Lists.newArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        Iterator listIterator = list.iterator();
//        System.out.println(listIterator.next());
//        System.out.println(listIterator.next());-***
        PeekingIterator peekingIterator = Iterators.peekingIterator(listIterator);
        System.out.println(peekingIterator.peek());
        System.out.println(peekingIterator.peek());
        System.out.println(peekingIterator.peek());
    }
}
