package com.xuanwoa.rabbit.guava.util;

import com.google.common.collect.AbstractIterator;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/20
 */
public class MyIterator<T> extends AbstractIterator<T> {
    //TODO 待研究，目前不懂具体使用方法
    @Override
    protected T computeNext() {
        while (this.hasNext()) {
            T t = this.next();
            if (t != null) {
                return t;
            }
        }
        return this.endOfData();
    }
}
