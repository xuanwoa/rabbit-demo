package com.xuanwoa.rabbit.guava.util;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/20
 */
public class MyList<T> extends ArrayList<T> implements Iterable<T> {
    public static void main(String[] args) {
        MyList<String> list = new MyList<>();
        list.add("A");
        list.add("B");
        list.add(null);
        list.add("C");
        list.add("D");
        MyIterator<String> iterator = (MyIterator<String>) list.iterator();
        System.out.println(iterator.hasNext());
        System.out.println(iterator.next());

    }
    @Override
    public Iterator<T> iterator() {
        return new MyIterator<>();
    }
}
