package com.xuanwoa.rabbit.guava.collection;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkElementIndex;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/16
 */
public class PreconditionsDemo {
    public static void main(String[] args) {
        checkElementIndex(4, 2, "wrong");
    }
}
