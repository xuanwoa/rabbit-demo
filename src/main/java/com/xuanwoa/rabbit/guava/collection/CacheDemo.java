package com.xuanwoa.rabbit.guava.collection;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.concurrent.ExecutionException;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/19
 */
@Slf4j
public class CacheDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Cache<Object, Object> cache = CacheBuilder.newBuilder()
                .maximumSize(10000)
                .expireAfterWrite(Duration.ofMillis(1))
                .removalListener(notification -> log.info("缓存被清除"))
                .build(new CacheLoader<Object, Object>() {
                    @Override
                    public Object load(Object key) {
                        return key;
                    }
                });
        cache.put("xuanwoa", 23);
        cache.put("test", 24);
        log.info(cache.get("xuanwoa", () -> "没有取到缓存").toString());
        log.info(cache.get("test", () -> "没有取到缓存").toString());
        cache.invalidate("xuanwoa");
    }
}
