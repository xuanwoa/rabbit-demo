package com.xuanwoa.rabbit.guava.collection;

import cn.hutool.core.lang.Console;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/19
 */
public class BiMapDemo {
    public static void main(String[] args) {
        HashBiMap<String,Integer> hashBiMap = HashBiMap.create();
        hashBiMap.put("xuanwoa", 23);
        hashBiMap.put("xuanwoa", 24);
        Console.log("hashBiMap[{}],value[{}]",hashBiMap,hashBiMap.get("xuanwoa"));
        BiMap<Integer, String> inverseBiMap = hashBiMap.inverse();
        System.out.println();
        Console.log("inverseBiMap[{}],value[{}]",inverseBiMap,inverseBiMap.get(23));
    }
}
