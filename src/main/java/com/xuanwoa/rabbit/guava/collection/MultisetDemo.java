package com.xuanwoa.rabbit.guava.collection;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.TreeMultiset;

/**
 * Description Multiset没有元素顺序限制的ArrayList
 *
 * @author caomin
 * @since 2021/1/19
 */
public class MultisetDemo {

    public static void main(String[] args) throws InterruptedException {
//        HashMultiset<Integer> hashMultiset = HashMultiset.create();
//        hashMultiset.add(2);
//        hashMultiset.add(1);
//        hashMultiset.add(4);
//        hashMultiset.add(7);
//        hashMultiset.add(7);
//
//        System.out.println(hashMultiset);
        TreeMultiset<Integer> treeMultiset = TreeMultiset.create();
        treeMultiset.add(2);
        treeMultiset.add(1);
        treeMultiset.add(4);
        treeMultiset.add(7);
        treeMultiset.add(7);
        System.out.println(treeMultiset);
    }
}
