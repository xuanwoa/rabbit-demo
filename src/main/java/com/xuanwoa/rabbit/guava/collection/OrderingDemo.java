package com.xuanwoa.rabbit.guava.collection;

import com.google.common.base.Function;
import com.google.common.collect.Ordering;
import org.checkerframework.checker.nullness.qual.Nullable;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/16
 */
public class OrderingDemo {
    public static void main(String[] args) {
        Ordering<String> result = Ordering.natural().nullsFirst().onResultOf(new Function<String, Integer>() {
            @Override
            public @Nullable Integer apply(@Nullable String input) {
                return null;
            }
        });
    }

}
