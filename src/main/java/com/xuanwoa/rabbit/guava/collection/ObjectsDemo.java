package com.xuanwoa.rabbit.guava.collection;

import com.google.common.base.Objects;
import lombok.Data;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/16
 */
public class ObjectsDemo {
    public static void main(String[] args) {
    }

    @Data
    static class User {
        String username;
        String password;

        public User() {
        }

        public User(String username, String password) {
            this.username = username;
            this.password = password;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            User user = (User) o;
            return Objects.equal(username, user.username) && Objects.equal(password, user.password);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(username, password);
        }
    }
}
