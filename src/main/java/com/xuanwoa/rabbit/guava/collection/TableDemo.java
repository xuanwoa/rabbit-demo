package com.xuanwoa.rabbit.guava.collection;

import cn.hutool.core.lang.Console;
import com.google.common.collect.ArrayTable;
import com.google.common.collect.HashBasedTable;

import java.util.Map;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/19
 */
public class TableDemo {
    public static void main(String[] args) {
        HashBasedTable<Integer, Integer, String> hashBasedTable = HashBasedTable.create();
        hashBasedTable.put(1, 1, "A");
        hashBasedTable.put(1, 2, "B");
        hashBasedTable.put(2, 1, "C");
        hashBasedTable.put(2, 2, "D");
        Console.log("hashBasedTable[{}]",hashBasedTable);
        Map<Integer, String> row1 = hashBasedTable.row(1);
        Map<Integer, String> row2 = hashBasedTable.row(2);
        Console.log("row 1 [{}]",row1);
        Console.log("row 2 [{}]",row2);
    }
}
