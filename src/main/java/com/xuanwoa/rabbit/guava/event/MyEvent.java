package com.xuanwoa.rabbit.guava.event;

import lombok.Data;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/21
 */
@Data
public class MyEvent {
    String name = "MyEvent";
}
