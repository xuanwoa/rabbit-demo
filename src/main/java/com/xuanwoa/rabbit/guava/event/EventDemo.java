package com.xuanwoa.rabbit.guava.event;

import com.google.common.eventbus.EventBus;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/21
 */
public class EventDemo {
    public static void main(String[] args) {
        EventBus eventBus = new EventBus();
        eventBus.register(new MyListener());
        eventBus.post(new MyEvent());
    }

}
