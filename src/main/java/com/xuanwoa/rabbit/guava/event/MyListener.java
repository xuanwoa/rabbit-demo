package com.xuanwoa.rabbit.guava.event;

import cn.hutool.core.lang.Console;
import com.google.common.eventbus.Subscribe;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/21
 */
public class MyListener {
    @Subscribe
    public void demo(MyEvent event) {
        Console.log("监听到事件[{}]", event.getName());
    }
}
