package com.xuanwoa.rabbit.guava.str;

import com.google.common.base.Splitter;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/21
 */
public class SplitterDemo {
    public static void main(String[] args) {
        String s1 = ",a,,b,";
        String[] split = s1.split(",");
        System.out.println(split.length);
        for (String s : split) {
            System.out.println(s);
        }
        System.out.println("-------------");
        Iterable<String> splitIterable = Splitter.on(",")
                .trimResults()
                .omitEmptyStrings()
                .split(s1);
        splitIterable.forEach(System.out::println);
    }
}
