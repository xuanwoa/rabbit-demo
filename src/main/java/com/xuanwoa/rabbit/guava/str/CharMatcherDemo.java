package com.xuanwoa.rabbit.guava.str;

import cn.hutool.core.lang.Console;
import com.google.common.base.CharMatcher;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/21
 */
public class CharMatcherDemo {
    public static void main(String[] args) {

    }

    /**
     * 移除control字符
     */
    private static void noControl() {
        String str = "hello world\n world hello";
        Console.log("str[{}]", str);
        String noControl = CharMatcher.javaIsoControl().removeFrom(str);
        Console.log("noControl[{}]", noControl);
    }

    /**
     * 去除两端的空格，并把中间的连续空格替换成单个空格
     */
    private static void spaced() {
        String str = " hello         world    ";
        Console.log("str[{}]", str);
        String spaced = CharMatcher.whitespace().trimAndCollapseFrom(str, ' ');
        Console.log("spaced[{}]", spaced);
    }
}
