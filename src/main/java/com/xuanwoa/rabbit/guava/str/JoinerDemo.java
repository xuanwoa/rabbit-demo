package com.xuanwoa.rabbit.guava.str;

import cn.hutool.core.lang.Console;
import com.google.common.base.Joiner;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/21
 */
public class JoinerDemo {
    public static void main(String[] args) {
        Joiner joiner = Joiner.on("; ").skipNulls();
        String joinStr = joiner.join("a", "b", "c", null, "d");
        Console.log("joinStr[{}]", joinStr);
    }
}
