package com.xuanwoa.rabbit.guava.cache;

import cn.hutool.core.util.RandomUtil;
import com.google.common.cache.CacheLoader;

import java.util.Map;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/20
 */
public class MyCacheLoader extends CacheLoader<String, Integer> {

    @Override
    public Integer load(String key) throws Exception {
        return RandomUtil.randomInt();
    }

    @Override
    public Map<String, Integer> loadAll(Iterable<? extends String> keys) throws Exception {
        return super.loadAll(keys);
    }
}
