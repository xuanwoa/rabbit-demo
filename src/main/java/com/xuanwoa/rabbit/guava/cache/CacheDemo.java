package com.xuanwoa.rabbit.guava.cache;

import cn.hutool.core.lang.Console;
import cn.hutool.core.util.RandomUtil;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;

import java.time.Duration;
import java.util.concurrent.ExecutionException;

/**
 * Description
 *
 * @author caomin
 * @since 2021/1/20
 */
public class CacheDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        LoadingCache<String, Integer> cache = CacheBuilder.newBuilder()
                .expireAfterWrite(Duration.ofMillis(500))
                .maximumSize(5)
                .build(new MyCacheLoader());

        for (int i = 0; i < 6; i++) {
            Console.log("cache stats [{}]", cache.stats());
            Console.log("cache[{}]", cache.get(RandomUtil.randomString(4)));
            printSize(cache);
        }
        cache.cleanUp();
        printSize(cache);
    }

    private static void printSize(Cache cache) {
        Console.log("cache size [{}]", cache.size());
    }
}
