package com.xuanwoa.rabbit.event;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

/**
 * Description
 *
 * @author: caomin
 * @since: 2020/11/30
 */
public class EventCenter {
    private final EventBus eventBus;
    public EventCenter() {
        eventBus = new EventBus();
    }

    public void register(Object object) {
        eventBus.register(object);
    }

    public void unregister(Object object) {
        eventBus.register(object);
    }

    public void post(Object event) {
        eventBus.post(event);
    }
}
