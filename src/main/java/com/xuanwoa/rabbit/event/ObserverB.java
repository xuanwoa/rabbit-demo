package com.xuanwoa.rabbit.event;

import com.google.common.eventbus.Subscribe;
import lombok.extern.slf4j.Slf4j;

/**
 * Description
 *
 * @author: caomin
 * @since: 2020/11/30
 */
@Slf4j
public class ObserverB {

    @Subscribe
    public void onMsg(String msg) {
        log.info(msg);
    }
}
