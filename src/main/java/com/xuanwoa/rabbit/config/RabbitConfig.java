package com.xuanwoa.rabbit.config;

import com.xuanwoa.rabbit.constant.RabbitConstant;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

import static com.xuanwoa.rabbit.constant.RabbitConstant.TAOBAO_QUEUE;

/**
 * Description
 *
 * @author: xuanwoa
 * @since: 2020/11/27
 */
@Configuration
public class RabbitConfig {
//
//    @Bean
//    public DirectExchange directExchange() {
//        return ExchangeBuilder.directExchange(RabbitConstant.DIRECT_EXCHANGE).build();
//    }
//
//    @Bean
//    public FanoutExchange fanoutExchange() {
//        return ExchangeBuilder.fanoutExchange(RabbitConstant.FANOUT_EXCHANGE).build();
//    }
//
//    @Bean
//    TopicExchange topicExchange() {
//        return ExchangeBuilder.topicExchange(RabbitConstant.TOPIC_EXCHANGE).build();
//    }
//
//    @Bean
//    public Queue directQueueA() {
//        return QueueBuilder.durable(RabbitConstant.DIRECT_QUEUE_A).build();
//
//    }
//
//    @Bean
//    public Queue fanoutQueueB() {
//        return QueueBuilder.durable(RabbitConstant.FANOUT_QUEUE_B).build();
//    }
//
//    @Bean
//    public Queue fanoutQueueC() {
//        return QueueBuilder.durable(RabbitConstant.FANOUT_QUEUE_C).build();
//    }
//
//    @Bean
//    public Queue topicQueueD() {
//        return QueueBuilder.durable(RabbitConstant.TOPIC_QUEUE_D).build();
//    }
//
//    @Bean
//    public Queue topicQueueE() {
//        return QueueBuilder.durable(RabbitConstant.TOPIC_QUEUE_E).build();
//    }
//
//    @Bean
//    public Binding directBinding() {
//        return BindingBuilder
//                .bind(directQueueA())
//                .to(directExchange())
//                .with("direct.key");
//    }
//
//    @Bean
//    public Binding fanoutBindingB() {
//        return BindingBuilder
//                .bind(fanoutQueueB())
//                .to(fanoutExchange());
//    }
//
//    @Bean
//    public Binding fanoutBindingC() {
//        return BindingBuilder
//                .bind(fanoutQueueC())
//                .to(fanoutExchange());
//    }
//
//    @Bean
//    public Binding topicBindingD() {
//        return BindingBuilder
//                .bind(topicQueueD())
//                .to(topicExchange())
//                .with("*.d");
//    }
//
//    @Bean
//    public Binding topicBindingE() {
//        return BindingBuilder
//                .bind(topicQueueE())
//                .to(topicExchange())
//                .with("*.e");
//    }

    @Bean
    public DirectExchange taobaoExchange() {
        return ExchangeBuilder.directExchange(RabbitConstant.TAOBAO_EXCHANGE).build();
    }

    @Bean
    public Queue taobaoQueue() {
        return QueueBuilder.durable(RabbitConstant.TAOBAO_QUEUE).build();
    }

    @Bean
    public Binding taobaoBinding() {
        return BindingBuilder
                .bind(taobaoQueue())
                .to(taobaoExchange())
                .with("");
    }

    // 配置默认的交换机
    @Bean
    public CustomExchange demoDelayExchange() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-delayed-type", "direct");
        //参数二为类型：必须是x-delayed-message
        return new CustomExchange("demo_delay_exchange", "x-delayed-message", true, false, args);
    }

    @Bean
    public Queue demoDelayQueue() {
        return QueueBuilder.durable("demo_delay_queue").build();
    }

    // 绑定队列到交换器
    @Bean
    public Binding binding() {
        return BindingBuilder.bind(demoDelayQueue()).to(demoDelayExchange()).with("").noargs();
    }
}
